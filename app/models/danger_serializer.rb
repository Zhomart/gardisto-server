class DangerSerializer < ActiveModel::Serializer
  # cached

  # delegate :cache_key, to: :object

  attributes :id, :created_at, :name, :level,
    :created_at_human, :level_human, 
    :coordinates, :address

end

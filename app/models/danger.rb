class Danger
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  field :level, type: Float

  field :coordinates, type: Array

  field :address, type: String

  include Geocoder::Model::Mongoid

  after_validation :make_reverse_geocode


  include ActionView::Helpers::DateHelper


  def created_at_human
    # created_at.strftime('%Y-%m-%d %H:%M:%S')
    distance_of_time_in_words_to_now(created_at) + " ago"
  end

  def level_human
    if level > 20
      'CRITICAL'
    elsif level > 15
      'DANGER'
    else
      'WARNING'
    end
  end

  def uniq_name_time
    [name, (created_at.to_i / 20).to_s].join('-')
  end

  def self.last_and_uniq
    criteria.order_by(created_at: -1).to_a.uniq { |d| d.uniq_name_time }
  end

  protected

  def make_reverse_geocode
    if self.address.blank?
      self.address = Geocoder.address(coordinates)
    end
  end
end

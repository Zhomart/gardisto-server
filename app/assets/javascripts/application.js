// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// require turbolinks
//= require bootstrap
//= require underscore
//= require gmaps/google

var update_dangers_container = function(){
  $.get('/dangers', function(response){
    var dangers = response.dangers;
    $('#dangers_container tbody').html('');
    for (var i=0;i<dangers.length;++i){
      var d = dangers[i];
      var row = $('<tr>');

      var time = $('<td>')
      time.append($('<a>', { html: d.created_at_human, href: '/dangers/'+d.id.$oid }));

      row.append($('<td>', { html: i + 1 }));
      row.append(time);
      row.append($('<td>', { html: d.name }));
      row.append($('<td>', { html: d.level_human + " ("+d.level+")" }));
      row.append($('<td>', { html: d.address }));

      $('#dangers_container tbody').append(row);
    }
  });

}

var createMap = function(lat, lng){
  handler = Gmaps.build('Google');
  handler.buildMap({ provider: { }, internal: {id: 'map'}}, function(){
    markers = handler.addMarkers([
      {
        "lat": lat,
        "lng": lng
      }
    ]);
    handler.bounds.extendWith(markers);
    handler.fitMapToBounds();
  });
}

$(function(){
  update_dangers_container();
  window.setInterval(update_dangers_container, 2000);


  if ($('#coordinates').length){
    var lat = $('#coordinates').data('lat');
    var lng = $('#coordinates').data('lng');

    createMap(lat, lng);
  }
});

class DangersController < ApplicationController
  respond_to :json # default to Active Model Serializers

  protect_from_forgery :except => :new

  def create
    Danger.create name: params[:name],
      level: params[:level],
      coordinates: [ params[:latitude].to_f, params[:longitude].to_f ]


    render json: { result: 'ok' }
  end

  def index
    dangers = Danger.last_and_uniq
    Rails.logger.info "Danger count: #{dangers.size}"
    render json: dangers
  end

  def show
    @danger = Danger.find(params[:id])
  end
end
